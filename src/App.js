import Routing from "./routes/router";
import "./App.css";

function App() {
  return <Routing></Routing>;
}

export default App;
