export const Search = async (apiUrl) => {
  try {
    return await fetch(`${process.env.REACT_APP_API_URL}/api/${apiUrl}`);
  } catch (err) {
    console.log(err);
  }
};

export const Create = async (apiUrl, body) => {
  try {
    return await fetch(`${process.env.REACT_APP_API_URL}/api/${apiUrl}`, {
      method: "POST",
      body: JSON.stringify(body),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });
  } catch (err) {
    console.log(err);
  }
};

export const Put = async (apiUrl, body) => {
  try {
    return await fetch(`${process.env.REACT_APP_API_URL}/api/${apiUrl}`, {
      method: "PUT",
      body: JSON.stringify(body),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });
  } catch (err) {
    console.log(err);
  }
};

export const Delete = async (apiUrl) => {
  try {
    return await fetch(`${process.env.REACT_APP_API_URL}/api/${apiUrl}`, {
      method: "DELETE",
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });
  } catch (err) {
    console.log(err);
  }
};
