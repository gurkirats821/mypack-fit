import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Auth from "../pages/auth/auth/auth";
import Login from "../pages/auth/login/login";
import Forgot from "../pages/auth/forgot/forgot";
import NotFound from "../components/not-found";

const Routing = () => {
  return (
    <div>
      <Router>
        <Routes>
          <Route path="/" element={<Auth />}>
            <Route path="/login" element={<Login />} />
            <Route path="/forgot" element={<Forgot />} />
          </Route>
          <Route element={<NotFound />} />
        </Routes>
      </Router>
    </div>
  );
};

export default Routing;
