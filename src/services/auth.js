import { Create } from "../http-handler/generic-api";

export const SignIn = (body) => {
  return Create("users/signIn", body);
};
