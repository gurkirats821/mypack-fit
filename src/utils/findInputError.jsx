export function findInputError(errors, name) {
  const filtered = Object.keys(errors)
    .filter((key) => key.includes(name))
    .reduce((cur, key) => {
      return Object.assign(cur, { error: errors[key] });
    }, {});
  return filtered;
}

// static error(errors: any, fieldName: string = "Field") {
//   if (!errors) return null;
//   for (let propertyName in errors) {
//     if (errors.hasOwnProperty(propertyName)) {
//       return ValidatorsService.getValidatorErrorMessage(propertyName, {
//         ...errors[propertyName],
//         fieldName,
//       });
//     }
//   }
//   return null;
// }
