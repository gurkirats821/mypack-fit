/*-------------------------------------------------------------------
|  🐼 React FC Form
|
|  🐯 Purpose: RENDERS FORM CONTEXT AND INPUTS
|
|  🐸 Returns:  JSX
*-------------------------------------------------------------------*/

import { Formik, Form } from "formik";
import { logInValidation } from "../../../utils/validation";
import { useMutation } from "@tanstack/react-query";
import { SignIn } from "../../../services/auth";
import FormField from "../../../components/form-field";

const Login = () => {
  const { mutateAsync: signIn } = useMutation(
    {
      mutationFn: (payload) => SignIn(payload)
    }
  )
  return (
    <div>
      <div className="text-4xl font-bold">Sign in</div>
      <div className="font-normal text-2xl my-4">Welcome back!</div>
      <div className="text-xl mb-4">
        Please enter your Email and Password to sign in
      </div>
      <Formik
        initialValues={{ email: "", password: "" }}
        validationSchema={logInValidation}
        onSubmit={async (values, { setSubmitting }) => {
          const formValue = { ...values, deviceId: 'string', deviceType: 'web' };
          const res = await signIn(formValue);
          console.log(res);
          setSubmitting(false);
        }}
      >
        <Form>
          <FormField label={'Email Address'} type={'text'} fieldName={'email'} placeholder='Email Address'></FormField>
          <FormField label={'Password'} type={'password'} fieldName={'password'} placeholder='Password'> </FormField>
          <button type="submit" className="theme-btn grey ">
            Submit
          </button>
        </Form>
      </Formik>
    </div>
  );
};

export default Login;
