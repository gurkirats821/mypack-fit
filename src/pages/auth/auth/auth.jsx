import { Outlet } from "react-router-dom";
import authBg from "../../../assets/images/auth-bg.png";

const Auth = () => {
  return (
    <div className="flex">
      <div className="w-1/2">
        <div
          className="bg-cover bg-center h-screen w-full"
          style={{ backgroundImage: `url(${authBg})` }}
        ></div>
      </div>

      <div className="w-1/2 flex flex-col justify-center items-center	">
        <Outlet />
      </div>
    </div>
  );
};

export default Auth;
