import { Field, ErrorMessage } from "formik";

const FormField = ({ fieldName, type, label,placeholder }) => {
  return (
    <div className="w-full flex-col flex relative mb-3">
      <label htmlFor={fieldName}>{label}</label>
      <Field name={fieldName} type={type} className="input-field" placeholder={placeholder} />
      <ErrorMessage component="div" name={fieldName}>{msg => <div className='validation-err-message'>{msg}</div>}</ErrorMessage>
    </div>
  )
}
export default FormField;
